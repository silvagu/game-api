import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { CreateGameDto } from './dto/create-game.dto';
import { Game } from './game.model';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { GameDto } from './dto/game.dto';
import { User } from '../auth/user.model';
import { CategoriesService } from '../categories/categories.service';
import { QueryGameDto } from './dto/query-games.dto';

@Injectable()
export class GamesService {

  constructor(
    @InjectModel(Game.name) private gameModel: Model<Game>,
    private categoriesService: CategoriesService
    ) {}

  async insertGame(createGameDto: CreateGameDto, user: User): Promise<GameDto> {
    const { categoryId, ...createGame } = createGameDto;
    const category = await this.categoriesService.findCategoryById(categoryId)
    const game = new this.gameModel({ ...createGame, owner: user, category });
    return Game.toDto(await game.save());
  }

  async updateGame(gameId: string, gameDto: CreateGameDto, ownerId: string): Promise<GameDto> {
    const { categoryId, ...updateGame } = gameDto;
    const game = await this.gameModel.findById(gameId);
    if (ownerId !== game.owner['_id'].toString()) {
      throw new UnauthorizedException('You do not own this game');
    }
    const category = await this.categoriesService.findCategoryById(categoryId)
    await game.updateOne({ ...updateGame, category });
    return Game.toDto(await this.gameModel.findById(gameId));
  }

  async getAllGames(query: QueryGameDto): Promise<GameDto[]> {
    const { price, categoryName } = query;
    return Game.toDtoList(await this.gameModel.find(GamesService.filter(price, categoryName)).exec());
  }

  async deleteGame(gameId: string, ownerId: string): Promise<void> {
    const game = await this.findGameByIdAndOwner(gameId, ownerId);
    const result = await this.gameModel.deleteOne({ _id: game.id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException('Could not delete the game.');
    }
  }

  async findGameByIdAndOwner(gameId: string, ownerId: string): Promise<Game> {
    const game = await this.gameModel.findById(gameId);
    if (!game) {
      throw new NotFoundException('Could not find this game.');
    }
    if (ownerId !== game.owner['_id'].toString()) {
      throw new UnauthorizedException('You do not own this game');
    }
    return game;
  }

  async findGameById(gameId: string): Promise<Game> {
    const game = await this.gameModel.findById(gameId);
    if (!game) {
      throw new NotFoundException('Could not find this game.');
    }
    return game;
  }

  private static filter(price?: number, categoryName?: string) {
    if (price && categoryName) {
      return { price, 'category.name': categoryName };
    }
    if (price) return { price };
    if (categoryName) return { 'category.name': categoryName };
  }
}
