import { ApiProperty } from '@nestjs/swagger';

export class QueryGameDto {
  @ApiProperty({ required: false})
  price: number;

  @ApiProperty({ required: false })
  categoryName: string;
}
