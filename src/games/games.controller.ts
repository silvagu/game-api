import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse, ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { GamesService } from './games.service';
import { GameDto } from './dto/game.dto';
import { CreateGameDto } from './dto/create-game.dto';
import { CategoryDto } from '../categories/dto/category.dto';
import { GetUser } from '../auth/decorator/get-user.decorator';
import { User } from '../auth/user.model';
import { AuthGuard } from '@nestjs/passport';
import { QueryGameDto } from './dto/query-games.dto';


@ApiTags('Games')
@Controller('games')
export class GamesController {
  constructor(private gamesService: GamesService) {}

  @Get()
  @ApiOperation({ summary: 'Get All Games' })
  @ApiOkResponse({ description: 'The found games', type: [GameDto] })
  getGames(@Query() query: QueryGameDto): Promise<GameDto[]> {
    return this.gamesService.getAllGames(query);
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Create A Game' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiCreatedResponse({
    description: 'Game has been successful created',
    type: GameDto,
  })
  createGame(@Body() createGameDto: CreateGameDto, @GetUser() user: User): Promise<GameDto> {
    return this.gamesService.insertGame(createGameDto, user);
  }

  @Patch(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Update A Game' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiCreatedResponse({
    description: 'Game has been successful updated',
    type: GameDto,
  })
  updateGame(@Param('id') gameId: string,
             @Body() createGameDto: CreateGameDto,
             @GetUser() user: User): Promise<GameDto> {
    const { id: ownerId } = user;
    return this.gamesService.updateGame(gameId, createGameDto, ownerId);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Delete A Game' })
  @ApiBadRequestResponse({ description: 'Not able to parse the request.' })
  @ApiNotFoundResponse({ description: 'Not able to delete the game.' })
  @ApiOkResponse({ description: 'The game has been successful deleted' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  deleteGame(@Param('id') gameId: string, @GetUser() user: User): Promise<void> {
    const { id: ownerId } = user;
    return this.gamesService.deleteGame(gameId, ownerId);
  }
}
