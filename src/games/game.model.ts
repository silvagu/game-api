import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { GameDto } from './dto/game.dto';
import * as mongoose from 'mongoose';

@Schema()
export class Game extends Document {
  @Prop({ required: true })
  title: string;

  @Prop()
  description: string;

  @Prop()
  price: number

  @Prop()
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }

  @Prop()
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }

  public static toDto(entity: Game): GameDto {
    const dto = new GameDto();
    dto.id = entity._id;
    dto.title = entity.title;
    dto.description = entity.description;
    dto.price = entity.price
    dto.category = entity.category['name']
    return dto;
  }

  public static toDtoList(entities: Game[]): GameDto[] {
    return entities.map(game => this.toDto(game));
  }
}

export const GameSchema = SchemaFactory.createForClass(Game);
