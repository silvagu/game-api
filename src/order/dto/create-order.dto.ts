import { ApiProperty } from '@nestjs/swagger';

class CreateGameOrderDto {
  @ApiProperty()
  gameId: string;

  @ApiProperty()
  quantity: number
}

export class CreateOrderDTO {
  @ApiProperty({ type: [CreateGameOrderDto] })
  games: CreateGameOrderDto[];
}
