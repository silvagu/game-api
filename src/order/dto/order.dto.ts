import { GameDto } from '../../games/dto/game.dto';
import { ApiProperty } from '@nestjs/swagger';

class GameOrderDto {
  @ApiProperty({ type: GameDto })
  game: GameDto;

  @ApiProperty()
  quantity: number;
}

export class OrderDto {
  @ApiProperty({ type: [GameOrderDto] })
  games: GameOrderDto[];

  @ApiProperty()
  totalPrice: number;

  @ApiProperty()
  created: string;

  @ApiProperty()
  owner: string;
}
