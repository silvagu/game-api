import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { OrderDto } from './dto/order.dto';
import { Game } from '../games/game.model';

@Schema()
export class Order extends Document {
  @Prop({ required: true })
  totalPrice: number;

  @Prop()
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }

  @Prop()
  games: [{
    game: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Game',
    },
    quantity: {
      type: mongoose.Schema.Types.Number,
      default: 1,
    },
  }]

  @Prop()
  created: {
    type: mongoose.Schema.Types.Date
  }

  public static toDto(entity: Order): OrderDto {
    const dto = new OrderDto();
    dto.games = entity.games.map(({game, quantity}) => ({
      game: Game.toDto(game as unknown as Game),
      quantity: quantity as unknown as number
    }));
    dto.totalPrice = entity.totalPrice;
    dto.created = entity.created.toString();
    dto.owner = entity.owner['username'];
    return dto;
  }

  public static toDtoList(entities: Order[]): OrderDto[] {
    return entities.map(order => this.toDto(order));
  }
}

export const OrderSchema = SchemaFactory.createForClass(Order);
