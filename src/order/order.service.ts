import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { Order } from './order.model';
import { CreateOrderDTO } from './dto/create-order.dto';
import { User } from '../auth/user.model';
import { GamesService } from '../games/games.service';
import { OrderDto } from './dto/order.dto';

@Injectable()
export class OrderService {
  constructor(@InjectModel(Order.name) private orderModel: Model<Order>,
              private gamesService: GamesService) {}

  async getOrdersByUserId(): Promise<OrderDto[]> {
    const orders = await this.orderModel
      .find()
      .populate('games.Game')
      .populate('User');

    if (!orders) {
      throw new NotFoundException('No Orders Found');
    }
    return Order.toDtoList(orders);
  }

  async createOrder(orderDTO: CreateOrderDTO, owner: User): Promise<OrderDto> {
    const games = [];
    let totalPrice = 0;
    for (const game of orderDTO.games) {
      const g = await this.gamesService.findGameById(game.gameId);
      totalPrice += game.quantity * g.price;
      games.push({ game: g, quantity: game.quantity });
    }
    const order = new this.orderModel({
      owner,
      games,
      totalPrice,
      created: OrderService.getLocalDateTime()
    }).populate('games');
    const { _id } = await order.save();
    return  Order.toDto(await this.orderModel.findById({ _id }));
  }

  private static getLocalDateTime() {
    return (new Date()).toISOString().slice(0, 19).replace(/-/g, "/").replace("T", " ");
  }
}
