import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { OrderService } from './order.service';
import { User } from '../auth/user.model';
import { GetUser } from '../auth/decorator/get-user.decorator';
import { OrderDto } from './dto/order.dto';
import { AuthGuard } from '@nestjs/passport';
import { CreateOrderDTO } from './dto/create-order.dto';


@ApiTags('Orders')
@Controller('order')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get All Order Of The User' })
  @ApiOkResponse({ description: 'The found orders', type: [OrderDto] })
  listOrders(): Promise<OrderDto[]> {
    return this.orderService.getOrdersByUserId();
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Order a game' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  createOrder(@Body() createOrderDTO: CreateOrderDTO, @GetUser() user: User): Promise<OrderDto> {
    return this.orderService.createOrder(createOrderDTO, user);
  }
}
