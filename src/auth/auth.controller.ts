import { Body, Controller, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { SignUpDto } from './dto/signup.dto';
import { TokenDto } from './dto/token.dto';
import { CredentialsDto } from './dto/credentials.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {

  constructor(private authService: AuthService) {}

  @Post('/signup')
  @ApiOperation({ summary: 'Sign up user' })
  @ApiCreatedResponse({ description: 'Sign up has been successful' })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiResponse({
    status: 409,
    description: 'Email already exists',
  })
  signUp(@Body() signUpDto: SignUpDto): Promise<void> {
    return this.authService.createUser(signUpDto);
  }

  @Post('/signin')
  @ApiOperation({ summary: 'Sign in user' })
  @ApiCreatedResponse({ description: 'JWT access token', type: TokenDto })
  @ApiUnauthorizedResponse({ description: 'Invalid credentials.' })
  signIn(@Body() credentialsDto: CredentialsDto): Promise<TokenDto> {
    return this.authService.signIn(credentialsDto);
  }
}
