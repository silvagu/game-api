import * as bcrypt from 'bcrypt';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { UserDto } from './dto/user.dto';
import * as mongoose from 'mongoose';

@Schema()
export class User extends Document {
  @Prop({ required: true })
  username: string;

  @Prop({ required: true, unique: true, index: true })
  email: string;

  @Prop({ required: true })
  password: string;

  @Prop()
  salt: string;

  public static toDto(entity: User): UserDto {
    const dto =  new UserDto();
    dto.id = entity._id;
    dto.username = entity.username;
    dto.email = entity.password;
    return dto;
  }
}

export const UserSchema = SchemaFactory.createForClass(User);
