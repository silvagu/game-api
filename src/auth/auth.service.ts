import * as bcrypt from 'bcrypt';
import { JwtPayload } from './jwt/jwt.payload';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { User } from './user.model';
import { JwtService } from '@nestjs/jwt';
import { SignUpDto } from './dto/signup.dto';
import { CredentialsDto } from './dto/credentials.dto';
import { TokenDto } from './dto/token.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    private jwtService: JwtService) {}

  async createUser(signUpDto: SignUpDto): Promise<void> {
    const { username, email, password } = signUpDto;
    const salt = await bcrypt.genSalt();

    const user = new this.userModel({
      username,
      email,
      salt,
      password: await AuthService.hashPassword(password, salt)
    })

    try {
      await user.save();
    } catch (e) {
      if (e.code === 11000) {
        throw new ConflictException('Email already exists.');
      }
      throw new InternalServerErrorException(e.message);
    }
  }

  async signIn(credentialsDto: CredentialsDto): Promise<TokenDto> {
    let email = null;

    try {
      email = await this.validateUserPassword(credentialsDto);
    } catch (e) {
      throw new UnauthorizedException('Invalid credentials.');
    }

    const payload: JwtPayload = { email };
    const token = await this.jwtService.sign(payload);
    return { token };
  }

  private static async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }

  private async validateUserPassword(credentialsDto: CredentialsDto): Promise<string> {
    const { email, password } = credentialsDto;
    const user = await this.userModel.findOne({ email }).exec();
    const hash = await bcrypt.hash(password, user.salt);
    if (user && (user.password === hash)) {
      return user.email;
    } else {
      return null;
    }
  }
}
