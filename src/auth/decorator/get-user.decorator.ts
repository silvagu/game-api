import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from '../user.model';

export const GetUser = createParamDecorator<User>(
  (data: unknown, context: ExecutionContext): User => {
    const request = context.switchToHttp().getRequest();
    return request.user;
  },
);
