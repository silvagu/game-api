import {
  Body,
  Controller, Delete,
  Get, Param, Patch,
  Post,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { CategoriesService } from './categories.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { CategoryDto } from './dto/category.dto';

@ApiTags('Categories')
@Controller('categories')
export class CategoriesController {
  constructor(private categoriesService: CategoriesService) {}

  @Get()
  @ApiOperation({ summary: 'Get All Categories' })
  @ApiOkResponse({ description: 'The found categories', type: [CategoryDto] })
  getCategories(): Promise<CategoryDto[]> {
    return this.categoriesService.getAllCategories();

  }

  @Post()
  @ApiOperation({ summary: 'Create A Category' })
  @ApiCreatedResponse({
    description: 'Category has been successful created',
    type: CategoryDto,
  })
  createCategory(@Body() categoryDto: CreateCategoryDto): Promise<CategoryDto> {
    return this.categoriesService.insertCategory(categoryDto);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update A Category' })
  @ApiBadRequestResponse({ description: 'Not able to parse the request.' })
  @ApiOkResponse({ description: 'Category has been successful updated' })
  updateCategory(
    @Param('id') categoryId: string,
    @Body() categoryDto: CreateCategoryDto): Promise<CategoryDto> {
    return this.categoriesService.updateCategory(categoryId, categoryDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete A Category' })
  @ApiBadRequestResponse({ description: 'Not able to parse the request.' })
  @ApiNotFoundResponse({ description: 'Not able to delete category.' })
  @ApiOkResponse({ description: 'Category has been successful deleted' })
  deleteCategory(@Param('id') categoryId: string): Promise<void> {
    return this.categoriesService.deleteCategory(categoryId);
  }

}
