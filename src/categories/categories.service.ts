import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Category } from './category.model';
import { Model } from 'mongoose';
import { CategoryDto } from './dto/category.dto';

@Injectable()
export class CategoriesService {

  constructor(@InjectModel(Category.name) private categoryModel: Model<Category>) {}

  async getAllCategories(): Promise<CategoryDto[]> {
    return Category.toDtoList(await this.categoryModel.find().exec());
  }

  async insertCategory(categoryDto: CreateCategoryDto): Promise<CategoryDto> {
    const category = new this.categoryModel(categoryDto);
    return Category.toDto(await category.save());
  }

  async updateCategory(categoryId: string, categoryDto: CreateCategoryDto): Promise<CategoryDto> {
    try {
      const category = await this.categoryModel.findById(categoryId).exec();
      category.name = categoryDto.name;
      category.description = categoryDto.description.length ? categoryDto.description : category.description;
      return Category.toDto(await category.save());
    } catch (e) {
      throw new BadRequestException('Not able to parse the request.');
    }
  }

  async deleteCategory(categoryId: string): Promise<void> {
    let result = null;
    try {
      result = await this.categoryModel.deleteOne({ _id: categoryId }).exec();
    } catch (e) {
      throw new BadRequestException('Not able to parse the request.');
    }
    if (result?.deletedCount === 0) {
      throw new NotFoundException('Not able to delete category.');
    }
  }

  async findCategoryById(categoryId: string): Promise<Category> {
    const category = await this.categoryModel.findById(categoryId);
    if (!category) {
      throw new NotFoundException('Could not find this category.');
    }
    return category;
  }
}
