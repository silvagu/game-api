import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { CategoryDto } from './dto/category.dto';

@Schema()
export class Category extends Document {
  @Prop({ required: true })
  name: string;

  @Prop()
  description: string;

  public static toDto(entity: Category): CategoryDto {
    const dto = new CategoryDto();
    dto.id = entity._id;
    dto.name = entity.name;
    dto.description = entity.description;
    return dto;
  }

  public static toDtoList(entities: Category[]): CategoryDto[] {
    return entities.map(category => this.toDto(category));
  }
}

export const CategorySchema = SchemaFactory.createForClass(Category);
