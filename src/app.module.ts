import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CategoriesModule } from './categories/categories.module';
import { AuthModule } from './auth/auth.module';
import { GamesModule } from './games/games.module';
import { OrderModule } from './order/order.module';
import * as config from 'config';


const dbConfig = config.get('db');

@Module({
  imports: [
    MongooseModule.forRoot(process.env.DB_URL || dbConfig.url),
    CategoriesModule,
    AuthModule,
    GamesModule,
    OrderModule]
})
export class AppModule {}
