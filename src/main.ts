import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import * as config from 'config';

async function bootstrap() {
  const serverConfig = config.get('server');
  const port = process.env.PORT || serverConfig.port;
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder()
    .setTitle('Swagger Game Api')
    .setDescription('Application API for Game apps')
    .setVersion('1.0')
    .addTag('Games', 'Endpoint related to games information.')
    .addTag('Categories', 'Endpoint related to games categories.')
    .addTag('Auth', 'Endpoint related to authentication and token generation.')
    .addTag('Orders', 'Endpoint related game orders.')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-docs', app, document);

  app.useGlobalPipes(new ValidationPipe({
    transform: true,
  }));
  await app.listen(port);
}
bootstrap();
