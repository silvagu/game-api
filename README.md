[![license](https://img.shields.io/github/license/ajaymache/travis-ci-with-github.svg?style=flat-square)](https://opensource.org/licenses/MIT)
[![nodejs](https://img.shields.io/badge/Server-Node.js-%23339933?style=flat-square&logo=node.js)](https://nodejs.org/)
[![mongodb](https://img.shields.io/badge/database-mongodb-orange?style=flat-square)](https://www.mongodb.com/)
[![typescript](https://img.shields.io/badge/-TypeScript-%23007ACC?style=flat-square&logo=TYPESCRIPT)](https://www.typescriptlang.org/)


## Description

Test task for nodejs.


## Installation

```bash
$ yarn install
```

Set the db url in config/default.yml file:

For mongo atlas cloud:

```
mongodb+srv://<username>:<password>@cluster0.afrnv.mongodb.net/<dbname>?retryWrites=true&w=majority
```

If you want to use the provider docker container, use the below url:

```
mongodb://localhost:27017/gameapi
```

## Running the app

```bash
# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Swagger API
```bash
http://localhost:3000/api-docs
```

![picture](docs/swagger_main.png)

#### Category Endpoint
> You can create, delete, update and get the category endpoint.
![picture](docs/swagger_category.png)

#### Game Endpoint
> You can create, delete, update, get and filter by price and category the game endpoint. To create, delete or update, you will need to provide a token. To create a game, you need to provide the game description as the category id.
![picture](docs/swagger_game.png)

#### Auth Endpoint
> You can sign up (create user) or sign in. Sign in will return the jwt token which you can you to make authenticated requests.
![picture](docs/swagger_auth.png)

#### Order Endpoint
> Your can make an order by passing the id of the games and the amount of each games. It also providers the get order which will retrieve all the orders made. To use all the methods of this endpoint, you will need to be authenticated.
![picture](docs/swagger_order.png) 

### DTO
Schema lists all the available dto to communicate between the client and api.
![picture](docs/swagger_dto.png)


## Docker build &nbsp;:whale:

``` 
docker-compose up -d
```

## Stay in touch

- Author - [Gustavo Silva](https://github/gusilva)

## License

  Nest is [MIT licensed](LICENSE).
